import React from 'react'

import Layout from "../components/layout"

const About = ({location}) => (
	<Layout location={location}>
		<div>
			<h1>About Us</h1>
			<p>Laoreet adipiscing nunc ut per parturient velit natoque justo a lectus laoreet placerat ipsum cubilia. In habitant et eu adipiscing nibh id adipiscing leo rhoncus a at dui iaculis suspendisse placerat praesent feugiat eu sapien. Scelerisque sociis eleifend eu parturient sit ad condimentum vestibulum condimentum fringilla scelerisque orci ultricies condimentum consectetur feugiat nam velit scelerisque ad. Vestibulum a nisi tortor egestas ullamcorper nam arcu fames sociis porta lacinia dapibus parturient mi condimentum vestibulum a proin dictumst curae ullamcorper vehicula diam parturient a id.</p>
		</div>
	</Layout>
)

export default About
